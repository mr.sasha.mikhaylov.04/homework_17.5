#include <iostream>
#include <cmath>

class Vector {

private:
    double x;
    double y;
    double z;

public:
    int VectorLength() {
        return sqrt(x*x + y*y + z*z);
    }

    Vector() : x(0), y(0), z(0) {

    }
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {

    }
};

int main()
{
    Vector vector(20, -10, 2);
    std::cout << vector.VectorLength();
}
